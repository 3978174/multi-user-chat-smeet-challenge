# Architecture
1) We use STOMP (WS) for messaging in our system. 
2) RabbitMQ is used as a message broker.
3) MariaDB is used as a SQL db for our solution
4) Once user send message, the message is persisted in the database and broadcasted to all subscribers of topic
5) Once user subscribe to topic, the user retrieves last N elements from the database and start receiving messages from topic.


# Client connection
WS URL: **ws://username:password@url:port/chats** 
<br/>
Subscribe path: **/topic/chat.{chatId}** 
<br/>
Send to topic path: **/chats/{chatId}** 
<br/>

For demo we use in-memory users to validate authorization. 
The list of available users with their credentials is provided in properties file.

# Future works:
1) Implement real registration/authentication process
2) Handle possible duplications or losses when user retrieves messages from the database and then receives messages from broker.
3) Use NoSQL database for better scalability
4) Cover with tests
5) Implement chat room management service 
   1) creating rooms 
   2) deleting rooms
   3) setting password for a room
   4) blocking users for a room