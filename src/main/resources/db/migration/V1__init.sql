create table chat_message
(
    id        varchar(255) not null primary key,
    author    varchar(255) not null,
    chat_id   varchar(255) not null,
    message   varchar(255) not null,
    timestamp bigint       not null
);

