package com.smeet.chat.db.model;

import com.smeet.chat.endpoints.model.Message;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Getter
@Setter
@FieldNameConstants
public class ChatMessage {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  private String id;

  private String author;
  private String message;
  private String chatId;
  private Long timestamp;

  public static ChatMessage from(String chatId, Message message) {
    ChatMessage chatMessage = new ChatMessage();
    chatMessage.setAuthor(message.getFrom());
    chatMessage.setChatId(chatId);
    chatMessage.setTimestamp(message.getTimestamp());
    chatMessage.setMessage(message.getText());
    return chatMessage;
  }

}
