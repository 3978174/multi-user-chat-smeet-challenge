package com.smeet.chat.db.repo;

import com.smeet.chat.db.model.ChatMessage;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, String> {


  @Query("from ChatMessage where chatId = :chatId order by timestamp desc")
  List<ChatMessage> getLastMessages(@Param("chatId") String chatId, Pageable pageable);
}
