package com.smeet.chat.config;

import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@ConfigurationProperties(prefix = "app")
public class AppData {

  @Setter
  private List<DummyUser> users;


  public List<UserDetails> getUserDetails() {
    return users.stream().map(u -> User.withDefaultPasswordEncoder()
        .username(u.getUsername())
        .password(u.getPassword())
        .roles()
        .build()).collect(Collectors.toList());
  }

  @Getter
  @Setter
  static class DummyUser {

    private String username;
    private String password;
  }

}
