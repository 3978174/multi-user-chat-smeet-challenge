package com.smeet.chat.endpoints;

import com.smeet.chat.endpoints.model.Message;
import com.smeet.chat.services.ChatExchangeService;
import java.security.Principal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ChatExchange {

  private final ChatExchangeService chatExchangeService;

  @Value("${app.chat.subscribe-page-size}")
  private Integer pageSize;

  @MessageMapping("/chats/{chatId}")
  public void sendMessage(@DestinationVariable String chatId, String msgBody, Principal principal) {
    try {
      Message message = new Message(principal.getName(), msgBody);
      chatExchangeService.sendMessage(chatId, message);
      log.info("Message {} has been sent to chat {} by {}", msgBody, chatId, principal.getName());
    } catch (Exception e) {
      log.error(
          "Message {} has not been sent to chat {} by {}",
          msgBody,
          chatId,
          principal.getName(),
          e
      );
    }
  }

  @SubscribeMapping("/topic/chat.{chatId}")
  public List<Message> chatInit(@DestinationVariable String chatId) {
    return chatExchangeService.getLastMessages(chatId, 0, pageSize);
  }
}
