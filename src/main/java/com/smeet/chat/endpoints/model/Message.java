package com.smeet.chat.endpoints.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.smeet.chat.db.model.ChatMessage;
import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class Message {

  private String id;
  private final String from;
  private final String text;
  private Long timestamp = System.currentTimeMillis();

  public static Message from(ChatMessage chatMessage) {
    Message message = new Message(chatMessage.getAuthor(), chatMessage.getMessage());
    message.setId(chatMessage.getId());
    message.setTimestamp(chatMessage.getTimestamp());
    return message;
  }
}