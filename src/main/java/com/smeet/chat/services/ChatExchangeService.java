package com.smeet.chat.services;

import com.smeet.chat.db.model.ChatMessage;
import com.smeet.chat.db.repo.ChatMessageRepository;
import com.smeet.chat.endpoints.model.Message;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class ChatExchangeService {

  private final SimpMessagingTemplate messagingTemplate;
  private final ChatMessageRepository chatMessageRepository;

  public void sendMessage(String chatId, Message message) {
    ChatMessage chatMessage = ChatMessage.from(chatId, message);
    chatMessageRepository.saveAndFlush(chatMessage);
    messagingTemplate.convertAndSend("/topic/chat." + chatId, message);
  }

  public List<Message> getLastMessages(String chatId, int page, int pageSize) {
    PageRequest pr = PageRequest.of(
        page,
        pageSize,
        Sort.by(Direction.DESC, ChatMessage.Fields.timestamp)
    );
    return chatMessageRepository.getLastMessages(chatId, pr).stream()
        .map(Message::from)
        .collect(Collectors.toList());
  }
}
